package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class CountryRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getCountry()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getCountry().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
