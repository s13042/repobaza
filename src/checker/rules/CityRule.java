package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class CityRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getCity()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getCity().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
