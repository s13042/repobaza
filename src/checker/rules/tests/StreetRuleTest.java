package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import checker.rules.StreetRule;
import domain.Address;


public class StreetRuleTest {

	StreetRule rule = new StreetRule();

	@Test
	public void checker_should_check_if_the_street_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_street_is_not_empty(){
		Address a = new Address();
		a.setStreet("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_street_is_not_null(){
		Address a = new Address();
		a.setStreet("Kwiatowa");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}