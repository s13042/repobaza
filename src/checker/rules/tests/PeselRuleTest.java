package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import checker.rules.PeselRule;
import domain.Person;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_pesel_is_not_null(){
		Person p = new Person();
		p.setPesel("12312312312");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_return_OK_if_has_correct_length(){
		Person p = new Person();
		p.setPesel("1231231");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_return_OK_if_has_only_number(){
		Person p = new Person();
		p.setPesel("1231231a");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}