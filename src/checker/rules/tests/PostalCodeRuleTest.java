package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import checker.rules.PostalCodeRule;
import domain.Address;


public class PostalCodeRuleTest {

	PostalCodeRule rule = new PostalCodeRule();

	@Test
	public void checker_should_check_if_the_postalCode_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_city_is_not_empty(){
		Address a = new Address();
		a.setPostalCode("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_postalCode_is_not_null(){
		Address a = new Address();
		a.setPostalCode("80-000");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}