package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.CountryRule;
import checker.rules.NameRule;
import domain.Address;


public class CountryRuleTest {

	CountryRule rule = new CountryRule();

	@Test
	public void checker_should_check_if_the_country_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_country_is_not_empty(){
		Address a = new Address();
		a.setCountry("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_country_is_not_null(){
		Address a = new Address();
		a.setCountry("Polska");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}