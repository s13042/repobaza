package checker.roles.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import checker.rules.SurnameRule;
import domain.Person;

public class SurnameRuleTest {

	SurnameRule rule = new SurnameRule();
	
	@Test
	public void checker_should_check_if_the_person_surname_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_surname_is_not_empty(){
		Person p = new Person();
		p.setSurname("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_surname_is_not_null(){
		Person p = new Person();
		p.setSurname("Jankowski");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}