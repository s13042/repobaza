package checker.roles.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.LocalNumberRule;
import checker.rules.NameRule;
import domain.Address;


public class LocalNumberRuleTest {

	LocalNumberRule rule = new LocalNumberRule();

	@Test
	public void checker_should_check_if_the_localNumber_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_localNumber_is_not_empty(){
		Address a = new Address();
		a.setLocalNumber("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_localNumber_is_not_null(){
		Address a = new Address();
		a.setLocalNumber("5");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}