package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.CountryRule;
import checker.rules.NameRule;
import domain.Address;


public class CityRuleTest {

	CountryRule rule = new  CountryRule()l

	@Test
	public void checker_should_check_if_the_city_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_city_is_not_empty(){
		Address a = new Address();
		a.setCity("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_city_is_not_null(){
		Address a = new Address();
		a.setCity("Gdańsk");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}