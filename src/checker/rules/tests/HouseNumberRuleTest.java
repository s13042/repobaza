package checker.rules.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.HouseNumberRule;
import checker.rules.NameRule;
import domain.Address;


public class HouseNumberRuleTest {

	HouseNumberRule rule = new HouseNumberRule();

	@Test
	public void checker_should_check_if_the_houseNumber_is_not_null(){
		Address a = new Address();
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));		
	}
	
	@Test
	public void checker_should_check_if_the_houseNumber_is_not_empty(){
		Address a = new Address();
		a.setHouseNumber("");
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_houseNumber_is_not_null(){
		Address a = new Address();
		a.setHouseNumber("1");;
		CheckResult result =rule.checkRule(a);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}