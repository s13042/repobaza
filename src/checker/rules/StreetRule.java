package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class StreetRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getStreet()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getStreet().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
