package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;
import domain.User;

public class LoginRule implements ICanCheckRule<User>{
	public CheckResult checkRule(User entity) {

		if(entity.getLogin()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getLogin().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(entity.getLogin().length() < 5)
			return new CheckResult("Login wymaga co najmniej 5 liter", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
