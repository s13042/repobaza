package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class LocalNumberRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getLocalNumber()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getLocalNumber().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(!entity.getLocalNumber().matches("-?\\d+(\\.\\d+)?"))
			return new CheckResult("Numer lokalu powinien być liczbą", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
