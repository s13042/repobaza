package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class SurnameRule implements ICanCheckRule<Person>{

	public CheckResult checkRule(Person entity) {

		if(entity.getSurname()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getSurname().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}

}