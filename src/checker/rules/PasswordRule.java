package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;
import domain.User;

public class PasswordRule implements ICanCheckRule<User>{
	public CheckResult checkRule(User entity) {

		if(entity.getPassword()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().length() < 8)
			return new CheckResult("Hasła jest za krótkie", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
