package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class PostalCodeRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getPostalCode()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getPostalCode().equals(""))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
