package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class PeselRule implements ICanCheckRule<Person>{

	public CheckResult checkRule(Person entity) {
		if(entity.getPesel()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getPesel().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(!entity.getPesel().matches("-?\\d+(\\.\\d+)?"))
			return new CheckResult("Pesel to same liczby a tutaj nie sa", RuleResult.Error);
		if(entity.getPesel().length() < 11)
			return new CheckResult("w Polsce Pesel ma 11 cyfr", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}

}