package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Address;

public class HouseNumberRule implements ICanCheckRule<Address>{
	public CheckResult checkRule(Address entity) {

		if(entity.getHouseNumber()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getHouseNumber().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(!entity.getHouseNumber().matches("-?\\d+(\\.\\d+)?"))
			return new CheckResult("Numer domu powinien być liczbą", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
