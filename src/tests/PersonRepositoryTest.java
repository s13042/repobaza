package tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.junit.*;
import org.mockito.*;

import domain.*;
import repositories.IRepository;
import repositories.impl.*;
import unitofwork.*;

public class PersonRepositoryTest {

	IRepository<Person> personReposi;

	@Mock
	private Map<Entity, IUnitOfWorkRepository> entities;
	PersonBuilder builder;
	Connection connection;
	Person person;

	@InjectMocks
	UnitOfWork uow;

	@Before
	public void initMocks() throws SQLException {
		uow = mock(UnitOfWork.class);
		MockitoAnnotations.initMocks(this);
		builder = mock(UserBuilder.class);
		connection = mock(Connection.class);
		when(connection.createStatement()).thenReturn(
				mock(java.sql.Statement.class));
		when(connection.prepareStatement(anyString())).thenReturn(
				mock(PreparedStatement.class));
		personReposi = new PersonRepository(connection, builder, uow);
		person = mock(Person.class);
	}

	@Test(expected = NullPointerException.class)
	public void test_save_with_null_as_argument() throws SQLException {

		personReposi.save(null);
	}

	@Test
	public void test_save_with_correct_argument() throws SQLException {

		doCallRealMethod().when(person).setState((EntityState) any());
		doCallRealMethod().when(person).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.save(person);

		assertSame(EntityState.New, person.getState());
	}

	@Test
	public void test_update_with_correct_argument() throws SQLException {

		doCallRealMethod().when(person).setState((EntityState) any());
		doCallRealMethod().when(person).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.save(person);

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.update(person);

		assertSame(EntityState.Changed, person.getState());

	}

	@Test(expected = IllegalStateException.class)
	public void test_update_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(person).setState((EntityState) any());
		doCallRealMethod().when(person).getState();

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.update(person);
	}

	@Test(expected = NullPointerException.class)
	public void test_update_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.update(null);
	}

	@Test(expected = IllegalStateException.class)
	public void test_delete_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.delete(person);
	}

	@Test(expected = NullPointerException.class)
	public void test_delete_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.delete(null);
	}

	@Test
	public void test_delete_with_correct_argument() throws SQLException {

		doCallRealMethod().when(person).setState((EntityState) any());
		doCallRealMethod().when(person).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.save(person);

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		personReposi.delete(person);

		assertSame(EntityState.Deleted, person.getState());

	}
}
