package tests;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import repositories.IRepository;
import repositories.impl.RoleBuilder;
import repositories.impl.RoleRepository;
import unitofwork.IUnitOfWorkRepository;
import unitofwork.UnitOfWork;
import domain.Entity;
import domain.EntityState;
import domain.Role;

public class RoleRepositoryTest {

	IRepository<Role> roleRepository;

	@Mock
	private Map<Entity, IUnitOfWorkRepository> entities;
	RoleBuilder builder;
	Connection connection;
	Role role;

	@InjectMocks
	UnitOfWork uow;

	@Before
	public void initMocks() throws SQLException {
		uow = mock(UnitOfWork.class);
		MockitoAnnotations.initMocks(this);
		builder = mock(RoleBuilder.class);
		connection = mock(Connection.class);
		when(connection.createStatement()).thenReturn(
				mock(java.sql.Statement.class));
		when(connection.prepareStatement(anyString())).thenReturn(
				mock(PreparedStatement.class));
		roleRepository = new RoleRepository(connection, builder, uow);
		role = mock(Role.class);

	}

	@Test(expected = NullPointerException.class)
	public void test_null_as_argument() throws SQLException {

		roleRepository.save(null);
	}

	@Test
	public void test_correct_argument() throws SQLException {

		doCallRealMethod().when(role).setState((EntityState) any());
		doCallRealMethod().when(role).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.save(role);

		assertSame(EntityState.New, role.getState());
	}

	@Test
	public void test_update_with_correct_argument() throws SQLException {

		doCallRealMethod().when(role).setState((EntityState) any());
		doCallRealMethod().when(role).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.save(role);

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.update(role);

		assertSame(EntityState.Changed, role.getState());

	}

	@Test(expected = IllegalStateException.class)
	public void test_update_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(role).setState((EntityState) any());
		doCallRealMethod().when(role).getState();

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.update(role);
	}

	@Test(expected = NullPointerException.class)
	public void test_update_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.update(null);
	}

	@Test(expected = IllegalStateException.class)
	public void test_delete_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.delete(role);
	}

	@Test(expected = NullPointerException.class)
	public void test_delete_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.delete(null);
	}

	@Test
	public void test_delete_with_correct_argument() throws SQLException {

		doCallRealMethod().when(role).setState((EntityState) any());
		doCallRealMethod().when(role).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.save(role);

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		roleRepository.delete(role);

		assertSame(EntityState.Deleted, role.getState());

	}

}