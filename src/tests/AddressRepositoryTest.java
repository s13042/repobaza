package tests;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import repositories.IRepository;
import repositories.impl.AddressBuilder;
import repositories.impl.AddressRepository;
import repositories.impl.RoleBuilder;
import repositories.impl.RoleRepository;
import unitofwork.IUnitOfWorkRepository;
import unitofwork.UnitOfWork;
import domain.Address;
import domain.Entity;
import domain.EntityState;
import domain.Role;

public class AddressRepositoryTest {

	IRepository<Address> addressRepository;

	@Mock
	private Map<Entity, IUnitOfWorkRepository> entities;
	AddressBuilder builder;
	Connection connection;
	Address address;

	@InjectMocks
	UnitOfWork uow;

	@Before
	public void initMocks() throws SQLException {
		uow = mock(UnitOfWork.class);
		MockitoAnnotations.initMocks(this);
		builder = mock(AddressBuilder.class);
		connection = mock(Connection.class);
		when(connection.createStatement()).thenReturn(
				mock(java.sql.Statement.class));
		when(connection.prepareStatement(anyString())).thenReturn(
				mock(PreparedStatement.class));
		addressRepository = new AddressRepository(connection, builder, uow);
		address = mock(Address.class);

	}

	@Test(expected = NullPointerException.class)
	public void test_null_as_argument() throws SQLException {

		addressRepository.save(null);
	}

	@Test
	public void test_correct_argument() throws SQLException {

		doCallRealMethod().when(address).setState((EntityState) any());
		doCallRealMethod().when(address).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.save(address);

		assertSame(EntityState.New, address.getState());
	}

	@Test
	public void test_update_with_correct_argument() throws SQLException {

		doCallRealMethod().when(address).setState((EntityState) any());
		doCallRealMethod().when(address).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.save(address);

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.update(address);

		assertSame(EntityState.Changed, address.getState());

	}

	@Test(expected = IllegalStateException.class)
	public void test_update_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(address).setState((EntityState) any());
		doCallRealMethod().when(address).getState();

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.update(address);
	}

	@Test(expected = NullPointerException.class)
	public void test_update_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDirty((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.update(null);
	}

	@Test(expected = IllegalStateException.class)
	public void test_delete_with_argument_which_is_not_in_database()
			throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.delete(address);
	}

	@Test(expected = NullPointerException.class)
	public void test_delete_with_null_as_argument() throws SQLException {

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.delete(null);
	}

	@Test
	public void test_delete_with_correct_argument() throws SQLException {

		doCallRealMethod().when(address).setState((EntityState) any());
		doCallRealMethod().when(address).getState();
		doCallRealMethod().when(uow).markAsNew((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.save(address);

		doCallRealMethod().when(uow).markAsDeleted((Entity) any(),
				(IUnitOfWorkRepository) any());

		addressRepository.delete(address);

		assertSame(EntityState.Deleted, address.getState());

	}

}