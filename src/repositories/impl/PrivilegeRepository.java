package repositories.impl;

import java.sql.Connection;
import java.sql.SQLException;

import repositories.IPrivilegeRepository;
import unitofwork.IUnitOfWork;
import domain.Privilege;

public class PrivilegeRepository 
extends Repository<Privilege> implements IPrivilegeRepository{

	public PrivilegeRepository(Connection connection, IEntityBuilder<Privilege> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected String getTableName() {
		return "privilege";
	}

	@Override
	protected String getUpdateQuery() {
		return 
				"UPDATE privilege SET (name)=(?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO privilege(name) VALUES(?)";   
	}


	@Override
	protected void setUpInsertQuery(Privilege p) throws SQLException {
		
		insert.setString(1, p.getName());	
	}

	@Override
	protected void setUpUpdateQuery(Privilege p) throws SQLException {
		update.setString(1, p.getName());
		update.setInt(2, p.getId());	
	}


}
