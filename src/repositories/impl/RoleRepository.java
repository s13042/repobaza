package repositories.impl;

import java.sql.Connection;
import java.sql.SQLException;

import repositories.IRoleRepository;
import unitofwork.IUnitOfWork;
import domain.Role;

public class RoleRepository 
extends Repository<Role> implements IRoleRepository{

	public RoleRepository(Connection connection, IEntityBuilder<Role> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected String getTableName() {
		return "role";
	}

	@Override
	protected String getUpdateQuery() {
		return 
				"UPDATE role SET (name)=(?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO role(name) VALUES(?)"; 
	}


	@Override
	protected void setUpInsertQuery(Role r) throws SQLException {
		
		insert.setString(1, r.getName());	
	}

	@Override
	protected void setUpUpdateQuery(Role r) throws SQLException {
		update.setString(1, r.getName());
		update.setInt(2, r.getId());
		
	}


}
