package repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Address;

public class AddressBuilder implements IEntityBuilder<Address> {

	@Override
	public Address build(ResultSet rs) throws SQLException {
		Address address = new Address();
		address.setCountry(rs.getString("country"));
		address.setCity(rs.getString("city"));
		address.setStreet(rs.getString("street"));
		address.setPostalCode(rs.getString("postalCode"));
		address.setHouseNumber(rs.getString("houseNumber"));
		address.setLocalNumber(rs.getString("localNumber"));
		address.setId(rs.getInt("id"));
		return address;
	}

}
