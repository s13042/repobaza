package repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Address;
import domain.Privilege;

public class PreviligeBuilder implements IEntityBuilder<Privilege> {

	@Override
	public Privilege build(ResultSet rs) throws SQLException {
		Privilege permission = new Privilege();
		permission.setName(rs.getString("name"));
		permission.setId(rs.getInt("id"));
		return permission;
	}

}
