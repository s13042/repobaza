package repositories.impl;

import java.sql.Connection;
import java.sql.SQLException;

import repositories.IAddressRepository;
import unitofwork.IUnitOfWork;
import domain.Address;

public class AddressRepository 
extends Repository<Address> implements IAddressRepository{

	public AddressRepository(Connection connection, IEntityBuilder<Address> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected String getTableName() {
		return "address";
	}

	@Override
	protected String getUpdateQuery() {
		return 
				"UPDATE Address SET (country, city, street, postalCode, houseNumber, localNumber)=(?,?,?,?,?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Address(country, city, street, postalCode, houseNumber, localNumber) VALUES(?,?,?,?,?,?)";  
	}


	@Override
	protected void setUpInsertQuery(Address address) throws SQLException {
		
		insert.setString(1, address.getCountry());
		insert.setString(2, address.getCity());
		insert.setString(3, address.getStreet());
		insert.setString(4, address.getPostalCode());
		insert.setString(5, address.getHouseNumber());
		insert.setString(6, address.getLocalNumber());	
	}

	@Override
	protected void setUpUpdateQuery(Address address) throws SQLException {
		update.setString(1, address.getCountry());
		update.setString(2, address.getCity());
		update.setString(3, address.getStreet());
		update.setString(4, address.getPostalCode());
		update.setString(5, address.getHouseNumber());
		update.setString(6, address.getLocalNumber());
		update.setInt(7, address.getId());
	}


}
