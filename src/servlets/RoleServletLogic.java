package servlets;

import repositories.IRepositoryCatalog;
import repositories.impl.RepositoryCatalogProvider;
import domain.Role;

public class RoleServletLogic {

	IRepositoryCatalog catalog;
	
	public RoleServletLogic() {
		catalog = RepositoryCatalogProvider.catalog();
	}
	
	public void addNewRole(HttpServletRequest request)
	{
		Role a = new Role();
		a.setName(request.getParameter("name"));
		catalog.getRoles().save(a);
		catalog.commit();
	}
	
	public String showRoleInhtmlForm()
	{
		String html = "<ol>";
		for(Role u: catalog.getRoles().getAll())
		{
			html+="<li>"
					+ u.getName()
					+ "</li>";
			
		}
		html+="</ol>";
		return html;
	}
}