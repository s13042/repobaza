package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import repositories.IRepositoryCatalog;
import repositories.impl.RepositoryCatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;


@WebServlet("/AddAdress")
public class AddAddressServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdressServletLogic logic = new AdressServletLogic();
	
    public AddAddressServlets() {
        super();
    }

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		logic.addNewAdress(request);
		response.sendRedirect("ShowAddress");
	}

}
