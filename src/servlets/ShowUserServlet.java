package servlets;

import java.io.IOException;

@WebServlet("/ShowUsers")
public class ShowUsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UserServletLogic logic = new UserServletLogic();
    public ShowUsersServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print(logic.showUsersInhtmlForm());
		
	}
}