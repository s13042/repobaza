package servlets;

import java.io.IOException;

@WebServlet("/ShowRole")
public class RoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	RoleServletLogic logic = new RoleServletLogic();
    public RoleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print(logic.showRoleInhtmlForm());
		
	}
}