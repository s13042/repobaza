package servlets;

import javax.servlet.http.HttpServletRequest;

import domain.Address;
import domain.User;
import repositories.IRepositoryCatalog;
import repositories.impl.RepositoryCatalogProvider;

public class AdressServletLogic {

	IRepositoryCatalog catalog;
	
	public AdressServletLogic() {
		catalog = RepositoryCatalogProvider.catalog();
	}
	
	public void addNewAdress(HttpServletRequest request)
	{
		Address a = new Address();
		a.setCity(request.getParameter("city"));
		a.setCountry(request.getParameter("country"));
		a.setHouseNumber(request.getParameter("houseNumber"));
		a.setPostalCode(request.getParameter("postalCode"));
		a.setStreet(request.getParameter("street"));
		catalog.getAddress().save(a);
		catalog.commit();
	}
	
	public String showAddressInhtmlForm()
	{
		String html = "<ol>";
		for(Address u: catalog.getAddress().getAll())
		{
			html+="<li>"
					+ u.getCountry()
					+ "</li>";
			html+="<li>"
					+ u.getCity()
					+ "</li>";
			html+="<li>"
					+ u.getStreet()
					+ "</li>";
			html+="<li>"
					+ u.getHouseNumber()
					+ "</li>";
			html+="<li>"
					+ u.getHouseNumber()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}