package servlets;

import repositories.IRepositoryCatalog;
import repositories.impl.RepositoryCatalogProvider;
import domain.Person;

public class PersonServletLogic {

	IRepositoryCatalog catalog;
	
	public PersonServletLogic() {
		catalog = RepositoryCatalogProvider.catalog();
	}
	
	public void addNewPerson(HttpServletRequest request)
	{
		Person person = new Person();
		person.setPesel(request.getParameter("pesel"));
		person.setFirstName(request.getParameter("firstName"));
		person.setSurname(request.getParameter("surname"));
		
		catalog.getPersons().save(person);
		catalog.commit();
	}
	
	public String showPersonInhtmlForm()
	{
		String html = "<ol>";
		for(Person u: catalog.getPersons().getAll())
		{
			html+="<li>"
					+ u.getFirstName()
					+ "</li>";
			html+="<li>"
					+ u.getSurname()
					+ "</li>";
			
			html+="<li>"
					+ u.getPesel()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}